package users

import (
	"net/http"

	"github.com/labstack/echo"
)

func Foo() string {
	return "Foo"
}

func Root(c echo.Context) error {
	return c.String(http.StatusOK, Foo())
}

func GetUser(c echo.Context) error {
	// User ID from path `users/:id`
	id := c.Param("id")
	return c.String(http.StatusOK, id)
}
func Show(c echo.Context) error {
	// Get team and member from the query string
	team := c.QueryParam("team")
	member := c.QueryParam("member")
	return c.String(http.StatusOK, "team:"+team+", member:"+member)
}
