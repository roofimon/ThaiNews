package news

import (
	"net/http"

	"github.com/labstack/echo"
	"github.com/mmcdole/gofeed"
)

var allNews = [2]string{
	"https://www.posttoday.com/rss/src/breakingnews.xml",
	"http://feeds.bbci.co.uk/news/rss.xml"}

//Parse news feed from newspaper
func Parse(c echo.Context) error {
	ni := parseRSSFeed(allNews[0])
	return c.JSON(http.StatusCreated, ni)
}

//Item represent rss item
type Item struct {
	Publised    string `json:"published"`
	Title       string `json:"title"`
	Description string `json:"description"`
	Link        string `json:"link"`
}

func newItem(i *gofeed.Item) Item {
	return Item{
		i.PublishedParsed.Format("2006-01-02 15:04:05"),
		i.Title,
		i.Description,
		i.Link}
}

func parseRSSFeed(source string) []Item {
	fp := gofeed.NewParser()
	f, err := fp.ParseURL(source)
	if err != nil {
		return []Item{}
	}
	return createItemSlice(f)
}

func createItemSlice(f *gofeed.Feed) []Item {
	var slices = []Item{}
	for _, i := range f.Items {
		slices = append(slices, newItem(i))
	}
	return slices
}
