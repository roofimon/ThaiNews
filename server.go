package main

import (
	"ThaiNews/news"
	"ThaiNews/users"

	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
)

func main() {
	e := echo.New()
	e.GET("/", users.Root)
	e.GET("/users/:id", users.GetUser)
	e.GET("/show", users.Show)
	e.GET("/news", news.Parse)

	//CORS
	e.Use(middleware.CORSWithConfig(middleware.CORSConfig{
		AllowOrigins: []string{"*"},
		AllowMethods: []string{echo.GET, echo.HEAD, echo.PUT, echo.PATCH, echo.POST, echo.DELETE},
	}))

	e.Logger.Fatal(e.Start(":1323"))
}
